## <div align="center"> Olá! <img src="./assets/emoji-assets/Hi.gif" alt="hi" width="29px"> Sou Eduardo Gomes! Seja bem vindo <𝚍𝚎𝚟𝚎𝚕𝚘𝚙𝚎𝚛𝚜/></div>

<div align="center">
<img src="./assets/snake.svg" alt="snake game" align="center" style="width: 100%" /> 
</div>  
  

### <div align="center"> Desenvolvedor full-stack👨‍💻 Estado civil: Em constante apredizado 🚀</div>  
  
- 😉 Du, Dudu e Edu são os melhores apelidos que um Geek poderia querer! Movido a
desafios, atualmente, estou aprendendo a programar com React, HTML, CSS, JavaScript,
SQL na Cubos Academy, como bolsista no Programa Cubos Academy e iFood.   
  
###
- ⚡ Familiarizado com sistemas operacionais como Windows, Android, Linux e plataformas
baseadas em Linux. Sou um cara flexível, que tem como missão de vida tornar a vida das
pessoas mais interessante e divertida através da tecnologia, é o que faz meus olhos
brilharem!

###
- 🔭 Eu nunca tive uma carreira de fato, sempre trabalhei onde tive oportunidade para sobreviver, atualmente sou entregador e vi nessa profissão a possibilidade de estudar e trabalhar para conquistar meus objetivos, ajudar em casa, buscar meu sustento.  
  
###
- 🌱A tecnologia me fez enxergar além, me fez sonhar com uma realidade diferente da que eu vivo hoje e finalmente construir uma carreira sólida... Meu objetivo é ficar longe das ruas, ter oportunidade de lazer, ter qualidade de vida, segurança física e financeira.

<br/>  



###### <div>Clique em<img src="./assets/emoji-assets/apontaor-indicador.png" width="29px"></div>

<details>
<table>
<tr>

<td>

## <div align="center" valign="top"> Linguagens de Programação </div>  

<div align="center">
<img style="margin: 10px" src="./assets/stack-assets/javascript_original_logo_icon_146455.svg" alt="javascript" height="70" />
<img style="margin: 10px" src="/assets/stack-assets/java_original_wordmark_logo_icon_146459.png" alt="java" height="80" />
<img style="margin: 10px;" src="./assets/stack-assets/typescript_original_logo_icon_146317.png" alt="typeScript" height="70" />
</div>

## <div align="center" valign="top"> Tecnologias </div>  

<div align="center">
<img style="margin: 10px" src="./assets/skill-assets/png/react_original_wordmark_logo_icon_146375.png" alt="react" height="85" />
<img style="margin: 10px" src="./assets/skill-assets/png/css_original_wordmark_logo_icon_146576.png" alt="css3" height="80" />
<img style="margin: 10px" src="./assets/skill-assets/png/html_original_wordmark_logo_icon_146478.png" alt="html5" height="80" />

<img style="margin: 10px" src="./assets/skill-assets/png/json_filetype_icon_177531.png" alt="json" height="80" />

<img style="margin: 10px" src="./assets/skill-assets/png/gitlab_original_wordmark_logo_icon_146504.png" alt="gitlab" height="80" />

<img style="margin: 10px" alt="github" height="80"  src="./assets/skill-assets/png/github_original_wordmark_logo_icon_146506.png" />
<img style="margin: 10px" src="./assets/skill-assets/png/express.png" alt="Express.js" height="40" />
<img style="margin: 10px" src="./assets/skill-assets/node-js-icon-8 (1).jpg" alt="Node.js" height="80" />
</div>


## <div align="center" valign="top">  Softwares </div>


<div align="center">  
<img style="margin: 10px" src="./assets/skill-assets/png/git_original_wordmark_logo_icon_146510.png" alt="Git" height="110" />
<img style="margin: 10px" alt="Visual Studio Code" height="80" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/docker-original-wordmark.svg" alt="Docker" height="80" />
<img style="margin: 10px" src="https://cdn.iconscout.com/icon/free/png-256/heroku-225989.png" alt="Heruku" height="80" />
<img style="margin: 10px" src="https://media.vlpt.us/images/ksmfou98/post/de191393-0b90-4f48-a753-f0a2606daae6/netlify-logo.png" alt="netlify" height="50" />

## <div align="center" valign="top">  Database </div>

<img style="margin: 10px" src="./assets/db-assets/postgresql_original_wordmark_logo_icon_146392.svg" alt="postgresql" height="85" />
<img style="margin: 10px" src="./assets/db-assets/mysql_original_wordmark_logo_icon_146417.svg" alt="mysql" height="100" />
<img style="margin: 10px" src="./assets/db-assets/icon_sql_256_30046.png" alt="sql" height="80" />


## <div align="center" valign="top" style="margin: 10px" >  Sistemas Operacionais </div>

<img style="margin: 10px" src="./assets/os-skill/linux_original_logo_icon_146433.png" alt="linux" height="80" />
<img style="margin: 10px" src="./assets/os-skill/4202097logomicrosoftmswindows-115591_115710.png" alt="windows" height="80" />
<img style="margin: 10px" src="./assets/os-skill/maclogo_244.png" alt="mac os" height="80" />
</div>

</td>


<td>

<div align="center">

 ## <div align="center" style="margin: 10px">Top Langs</div>
 
<div align="center" style="display: inline_block">
  <a align="center" href="https://github.com/Eduardo377">
    <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Eduardo377&layout=compact&langs_count=7&theme=dark"/>
  </a>
</div>

## <div align="center"  style="margin: 10px">Github Stats</div>
 
<div align="center" style="display: inline_block">
  <a align="center" href="https://github.com/Eduardo377">
    <img height="150em" src="https://github-readme-stats.vercel.app/api?username=Eduardo377&show_icons=true&theme=dark&include_all_commits=true&count_private=true"/>
  </a>
</div>

</div>

<div align="center">

## <div> Formação Acadêmica</div>

  <img width="280" src="https://i.imgur.com/LpY2nT4.png" alt="gama academy"  style="border-radius: 80px" />

  <img width="280" src="https://capitaldigital.com.br/wp-content/uploads/2020/09/kzv4wwbbh6sai7cwqyu8.jpg" alt="cubos academy"  style="border-radius: 80px" />

</div>

</td>
</tr>
</table>

</details>

## <div align="center"> Portfólio </div>

 <div align="center">  

   
  [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Eduardo377&repo=Cubos_Flix&show_owner=true)](https://github.com/Eduardo377/Cubos_Flix)
  [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Eduardo377&repo=Sistema-de-controle-financeiro-e-cobrancas&show_owner=true)](https://github.com/Eduardo377/Sistema-de-controle-financeiro-e-cobrancas)
  [![Readme Card](https://github-readme-stats.vercel.app/api/pin/?username=Eduardo377&repo=layout-sistema-de-controle-financeiro-e-cobrancas&show_owner=true)](https://github.com/Eduardo377/layout-sistema-de-controle-financeiro-e-cobrancas)
 </div>

## <div align="center"> Connect with me  </div>


<div align="center">
<a href="https://github.com/Eduardo377" target="_blank">
<img src="https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white" alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://codepen.com/eduardo377" target="_blank">
<img src="https://img.shields.io/badge/codepen-%23131417.svg?&style=for-the-badge&logo=codepen&logoColor=white" alt=codepen style="margin-bottom: 5px;" />
</a>
<a href="https://linkedin.com/in/eduardogomes377" target="_blank">
<img src="https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" alt="linkedin" style="margin-bottom: 5px;" />
</a>
<a href = "mailto: eduardogome377@gmail.com" target="_blank">
<img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" alt=gmail style="margin-bottom: 5px;">
</a>
<a href="https://discord.com/channels/#8875/" target="_blank">
<img src=https://i.redd.it/0utml758tfrx.png
 alt="discord" width="89px;" style="margin-bottom: 5px;" />
</a>
<a href="https://pt.stackoverflow.com/users/259231/eduardo377" target="_blank">
<img src="https://stackoverflow.design/assets/img/logos/so/logo-stackoverflow.png"
 alt="Stack Overflow" width="150px;" style="margin-bottom: 5px;" />
</a>
</div>  

<br/>

<div align="center">
<img src="https://spotify-github-profile.vercel.app/api/view.svg?uid=4i9vjeriexk34pxkdf4aykpcp&cover_image=false&theme=default&bar_color=53b14f&bar_color_cover=true" />
</div>  

<br/>  

<div align="center">
<img src="https://komarev.com/ghpvc/?username=Eduardo377&&style=flat-square" align="center" />
</div>
<br />

----
